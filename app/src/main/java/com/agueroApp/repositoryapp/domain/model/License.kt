package com.agueroApp.repositoryapp.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class License (
    var key: String = "",
    var name: String = "",
    var spdx_id: String = "",
    var url: String = "",
    var node_id: String = ""
): Parcelable