package com.agueroApp.repositoryapp.domain

import com.agueroApp.repositoryapp.domain.model.Repos
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface WebService {

    @Headers("Content-Type: application/json")
    @GET("repositories")
    suspend fun getRepositories(
        @Query("q") language : String,
        @Query("per_page") items : Int,
        @Query("page") page : Int) : Repos
}