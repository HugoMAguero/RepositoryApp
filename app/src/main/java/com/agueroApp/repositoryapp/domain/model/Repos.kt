package com.agueroApp.repositoryapp.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Repos (
    @SerializedName("total_count")
    var total_count: Int = 0,
    @SerializedName("incomplete_results")
    var incomplete_results: Boolean = false,
    @SerializedName("items")
    var items: List<Item> = listOf()
) : Parcelable