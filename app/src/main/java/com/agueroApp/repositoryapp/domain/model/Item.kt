package com.agueroApp.repositoryapp.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Item (
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("node_id")
    var node_id: String = "",
    @SerializedName("full_name")
    var full_name: String = "",
    @SerializedName("private")
    var private: Boolean = false,
    @SerializedName("owner")
    var owner: Owner = Owner(),
    @SerializedName("html_url")
    var html_url: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("fork")
    var fork: Boolean = false,
    @SerializedName("url")
    var url: String = "",
    @SerializedName("forks_url")
    var forks_url: String = "",
    @SerializedName("keys_url")
    var keys_url: String = "",
    @SerializedName("collaborators_url")
    var collaborators_url: String = "",
    @SerializedName("teams_url")
    var teams_url: String = "",
    @SerializedName("hooks_url")
    var hooks_url: String = "",
    @SerializedName("issue_events_url")
    var issue_events_url: String = "",
    @SerializedName("assignees_url")
    var assignees_url: String = "",
    @SerializedName("branches_url")
    var branches_url: String = "",
    @SerializedName("tags_url")
    var tags_url: String = "",
    @SerializedName("blobs_url")
    var blobs_url: String = "",
    @SerializedName("git_tags_url")
    var git_tags_url: String = "",
    @SerializedName("git_refs_url")
    var git_refs_url: String = "",
    @SerializedName("trees_url")
    var trees_url: String = "",
    @SerializedName("statuses_url")
    var statuses_url: String = "",
    @SerializedName("languages_url")
    var languages_url: String = "",
    @SerializedName("stargazers")
    var stargazers: String = "",
    @SerializedName("contributors_url")
    var contributors_url: String = "",
    @SerializedName("subscribers_url")
    var subscribers_url: String = "",
    @SerializedName("subscription_url")
    var subscription_url: String = "",
    @SerializedName("commits_url")
    var commits_url: String = "",
    @SerializedName("git_commits_url")
    var git_commits_url: String = "",
    @SerializedName("comments_url")
    var comments_url: String = "",
    @SerializedName("issue_comment_url")
    var issue_comment_url: String = "",
    @SerializedName("contents_url")
    var contents_url: String = "",
    @SerializedName("compare_url")
    var compare_url: String = "",
    @SerializedName("merges_url")
    var merges_url: String = "",
    @SerializedName("archive_url")
    var archive_url: String = "",
    @SerializedName("downloads_url")
    var downloads_url: String = "",
    @SerializedName("issues")
    var issues: String = "",
    @SerializedName("pulls_url")
    var pulls_url: String = "",
    @SerializedName("milestones_url")
    var milestones_url: String = "",
    @SerializedName("notifications_url")
    var notifications_url: String = "",
    @SerializedName("labels_url")
    var labels_url: String = "",
    @SerializedName("releases_url")
    var releases_url: String = "",
    @SerializedName("deployments_url")
    var deployments_url: String = "",
    @SerializedName("created_at")
    var created_at: Date = Date(),
    @SerializedName("updated_at")
    var updated_at: Date = Date(),
    @SerializedName("pushed_at")
    var pushed_at: Date = Date(),
    @SerializedName("git_url")
    var git_url: String = "",
    @SerializedName("ssh_url")
    var ssh_url: String = "",
    @SerializedName("clone_url")
    var clone_url: String = "",
    @SerializedName("svn_url")
    var svn_url: String = "",
    @SerializedName("homepage")
    var homepage: String = "",
    @SerializedName("size")
    var size: Int = 0,
    @SerializedName("stargazers_count")
    var stargazers_count: Int = 0,
    @SerializedName("watchers_count")
    var watchers_count: Int = 0,
    @SerializedName("language")
    var language: String = "",
    @SerializedName("has_issues")
    var has_issues: Boolean = false,
    @SerializedName("has_projects")
    var has_projects: Boolean = false,
    @SerializedName("has_downloads")
    var has_downloads: Boolean = false,
    @SerializedName("has_wiki")
    var has_wiki: Boolean = false,
    @SerializedName("has_pages")
    var has_pages: Boolean = false,
    @SerializedName("forks_counts")
    var forks_counts: Int = 0,
    @SerializedName("mirror_url")
    var mirror_url: String = "",
    @SerializedName("archived")
    var archived: Boolean = false,
    @SerializedName("disabled")
    var disabled: Boolean = false,
    @SerializedName("open_issues_counts")
    var open_issues_counts: Int = 0,
    @SerializedName("license")
    var license: License = License(),
    @SerializedName("allow_forking")
    var allow_forking: Boolean = false,
    @SerializedName("is_template")
    var is_template: Boolean = false,
    @SerializedName("web_commit_signoff_required")
    var web_commit_signoff_required: Boolean = false,
    @SerializedName("topics")
    var topics: List<String> = listOf(),
    @SerializedName("visibility")
    var visibility: String = "",
    @SerializedName("forks")
    var forks: Int = 0,
    @SerializedName("open_issues")
    var open_issues: Int = 0,
    @SerializedName("watchers")
    var watchers: Int = 0,
    @SerializedName("default_branch")
    var default_branch: String = "",
    @SerializedName("score")
    var score: Int = 0
): Parcelable