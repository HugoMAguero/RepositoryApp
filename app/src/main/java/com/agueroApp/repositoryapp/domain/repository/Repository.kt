package com.agueroApp.repositoryapp.domain.repository

import com.agueroApp.repositoryapp.domain.model.Repos
import com.agueroApp.repositoryapp.vo.Resource

interface Repository {
    suspend fun getRepositories(language: String, item: Int, page: Int): Resource<Repos>
}