package com.agueroApp.repositoryapp.ui.Main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.agueroApp.repositoryapp.R
import com.agueroApp.repositoryapp.base.BaseViewHolder
import com.agueroApp.repositoryapp.domain.model.Item
import com.agueroApp.repositoryapp.domain.model.Repos
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.rv_item_repos.view.*

class MainReposAdapter(private val context: Context,
                       private val items: List<Item>,
                       private val itemClick: OnClick): RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnClick {
        fun onClick(item: Item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return RepoViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_item_repos, parent, false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder) {
            is RepoViewHolder -> {
                holder.bind(items[position], position)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class RepoViewHolder(itemView: View): BaseViewHolder<Item>(itemView) {
        override fun bind(item: Item, position: Int) {
            itemView.name.text = item.full_name
            itemView.description.text = item.description
            Glide.with(context)
                .load(item.owner.avatar_url)
                .circleCrop()
                .into(itemView.avatar)
            itemView.setOnClickListener {
                itemClick.onClick(item)
            }
        }
    }
}