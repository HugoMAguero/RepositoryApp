package com.agueroApp.repositoryapp.ui.Main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.agueroApp.repositoryapp.R
import com.agueroApp.repositoryapp.data.data.DataSource
import com.agueroApp.repositoryapp.data.repository.RepositoryImplement
import com.agueroApp.repositoryapp.domain.model.Item
import com.agueroApp.repositoryapp.domain.model.Repos
import com.agueroApp.repositoryapp.viewModel.VMFactory
import com.agueroApp.repositoryapp.vo.Resource
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(), MainReposAdapter.OnClick {

    private val viewModel by viewModels<MainViewModel> {
        VMFactory(
            RepositoryImplement(
                DataSource()
            ))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

        viewModel.getRepos("kotlin", 30, 1).observe(viewLifecycleOwner, Observer {
            result ->
            when(result) {
                is Resource.Loading -> {
                    progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    progressBar.visibility = View.GONE
                    rv_repos.adapter = MainReposAdapter(requireContext(), result.data.items, this)
                }
                is Resource.Failure -> {
                    progressBar.visibility = View.GONE
                }
            }
        })
    }

    fun setupRecyclerView() {
        rv_repos.layoutManager = LinearLayoutManager(requireContext())
        rv_repos.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun onClick(item: Item) {
        val bundle = Bundle()
        bundle.putString("title", item.full_name)
        bundle.putString("description", item.description)
        bundle.putString("avatar", item.owner.avatar_url)
        findNavController().navigate(R.id.action_mainFragment_to_detailsFragment, bundle)
    }
}