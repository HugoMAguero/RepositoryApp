package com.agueroApp.repositoryapp.ui.Details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.agueroApp.repositoryapp.R
import com.agueroApp.repositoryapp.domain.model.Item
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.rv_item_repos.view.*

class DetailsFragment : Fragment() {

    private lateinit var title: String
    private lateinit var description: String
    private lateinit var avatar: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments.let {
            if (it != null) {
                title = it.getString("title")!!
                description = it.getString("description")!!
                avatar = it.getString("avatar")!!
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setItem(title, description, avatar)
    }

    private fun setItem(title: String, description: String, avatar :String) {
        title_detail.text = title
        text_instructions_detail.text = description
        Glide.with(requireContext())
            .load(avatar)
            .into(imageViewItem)
    }
}