package com.agueroApp.repositoryapp.ui.Main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.agueroApp.repositoryapp.domain.repository.Repository
import com.agueroApp.repositoryapp.vo.Resource
import kotlinx.coroutines.Dispatchers

class MainViewModel(private val repo: Repository) : ViewModel() {

    fun getRepos(language: String, item: Int, page: Int) = liveData(Dispatchers.IO) {
        emit(Resource.Loading)
        try {
            emit(repo.getRepositories(language, item, page))
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }
    }
}