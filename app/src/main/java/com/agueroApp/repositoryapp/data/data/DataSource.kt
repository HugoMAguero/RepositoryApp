package com.agueroApp.repositoryapp.data.data

import com.agueroApp.repositoryapp.domain.model.Repos
import com.agueroApp.repositoryapp.vo.Resource
import com.agueroApp.repositoryapp.vo.RetrofitClient

class DataSource() {
    suspend fun getRepositories(language : String, items: Int, page: Int) : Resource<Repos>  {
        return Resource.Success(RetrofitClient.webService.getRepositories(language, items, page))
    }
}