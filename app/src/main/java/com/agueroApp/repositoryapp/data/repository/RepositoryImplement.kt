package com.agueroApp.repositoryapp.data.repository

import com.agueroApp.repositoryapp.data.data.DataSource
import com.agueroApp.repositoryapp.domain.model.Repos
import com.agueroApp.repositoryapp.domain.repository.Repository
import com.agueroApp.repositoryapp.vo.Resource

class RepositoryImplement(private val dataSource: DataSource): Repository {

    override suspend fun getRepositories(
        language: String,
        item: Int,
        page: Int
    ): Resource<Repos> {
        return dataSource.getRepositories(language, item, page)
    }
}